package conjuntos;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ConjuntoTest {

	Conjunto conA;
	Conjunto conB;

	@Before
	public void setUp() throws Exception {
		conA = new Conjunto(3);
		conB = new Conjunto(7);

		conA.agregarItem("Item1");
		conA.agregarItem("Item3", 2);

		conB.agregarItem("Item4", 3);
		conB.agregarItem("Item1");
		conB.agregarItem("Item2", 1);
		conB.agregarItem("Item?");
		conB.agregarItem("zzzzz", 5);
		conB.agregarItem("Item6", 5);
		conB.agregarItem("Item?");
	}

	@Test
	public void testDimension() {
		assertTrue(conA.dimension() == 3);
		assertTrue(conB.dimension() == 7);
	}

	@Test
	public void testCardinal() {
		assertTrue(conA.cardinal() == 2);
		assertTrue(conB.cardinal() == 6);
	}

	@Test
	public void testDespliegue() {
		System.out.println("Desplegando conjunto vacio");
		Conjunto.VACIO.desplegar();
		System.out.println("Desplegando conjunto A");
		conA.desplegar();
		System.out.println("Desplegando conjunto B");
		conB.desplegar();
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void testAgregarItems() {
		conB.agregarItem("testItem", 7);
	}

	@Test
	public void testUnion() {
		// A UNION B = AUB
		Conjunto union1 = conA.union(conB);
		Conjunto union2 = conB.union(conA);
		assertTrue(union1.esIgualA(union2));
		assertFalse(conA.esIgualA(union1));
		// A UNION vacio = A
		Conjunto union3 = conA.union(Conjunto.VACIO);
		assertTrue(union3.esIgualA(conA));
	}
	
	@Test
	public void testInterseccion() {
		// A INTERSEC B = A^B
		Conjunto interseccion1 = conA.interseccion(conB);
		Conjunto interseccion2 = conB.interseccion(conA);
		assertTrue(interseccion1.esIgualA(interseccion2));
		assertFalse(interseccion1.esIgualA(conA));
		// A INTERSEC vacio = vacio
		Conjunto interseccion3 = conA.interseccion(Conjunto.VACIO);
		assertTrue(interseccion3.esIgualA(Conjunto.VACIO));
	}
	
	@Test
	public void testDiferencia() {
		// A DIFF B = A-B
		Conjunto diferencia1 = conA.diferencia(conB);
		Conjunto diferencia2 = conB.diferencia(conA);
		assertFalse(diferencia1.esIgualA(diferencia2));
		// Comparaciones
		Conjunto comparacionConA = new Conjunto(diferencia1.getStrings().length);
		comparacionConA.agregarItem("Item3");
		assertTrue(diferencia1.esIgualA(comparacionConA));
		Conjunto comparacionConB = new Conjunto(diferencia2.getStrings().length);
		comparacionConB.agregarItem("Item4");
		comparacionConB.agregarItem("Item?");
		comparacionConB.agregarItem("Item2");
		comparacionConB.agregarItem("Item?");
		comparacionConB.agregarItem("Item6");
		assertTrue(diferencia2.esIgualA(comparacionConB));
		// A DIFF vacio = A
		Conjunto diferencia3 = conA.diferencia(Conjunto.VACIO);
		assertTrue(conA.esIgualA(diferencia3));
	}
	
	@Test
	public void testVacioIgualAConstVacio() {
		Conjunto vacio = new Conjunto();
		assertTrue(vacio.esIgualA(Conjunto.VACIO));
	}
	
}
