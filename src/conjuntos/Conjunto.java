package conjuntos;

import java.util.ArrayList;

public class Conjunto {
	public static final Conjunto VACIO = new Conjunto();
	private static final int defaultConjuntoSize = 5; // Cambiar a gusto.
														// Default: 5
	private String[] strings;

	/**
	 * Instancia un conjunto de {@link String} de longitud {@code size}
	 * 
	 * @param size
	 */
	public Conjunto(int size) {
		initConjuntoStrings(size);
	}

	/**
	 * Instancia un conjunto de {@link String} con longitud por default.
	 */
	public Conjunto() {
		initConjuntoStrings(defaultConjuntoSize);
	}

	/**
	 * Retorna el conjunto union de el Conjunto con el del parametro.
	 * 
	 * @param conjunto
	 */
	public Conjunto union(Conjunto otroConjunto) {
		ArrayList<String> otroStringList = new ArrayList<String>();
		ArrayList<String> resultList = new ArrayList<String>();
		String[] otroStrings = otroConjunto.getStrings();
		for (String string : otroStrings) {
			if (string != null) {
				otroStringList.add(string);
				resultList.add(string);
			}
		}
		for (String string : strings) {
			if (string != null && !otroStringList.contains(string)) {
				resultList.add(string);
			}
		}
		Conjunto joint = new Conjunto(resultList.size());
		for (String string : resultList) {
			joint.agregarItem(string);
		}
		return joint;
	}

	/**
	 * Retorna el conjunto interseccion de el Conjunto con el del parametro.
	 * 
	 * @param intersection
	 */
	public Conjunto interseccion(Conjunto otroConjunto) {
		ArrayList<String> otroStringList = new ArrayList<String>();
		ArrayList<String> resultList = new ArrayList<String>();
		String[] otroStrings = otroConjunto.getStrings();
		for (String string : otroStrings) {
			otroStringList.add(string);
		}
		for (String string : strings) {
			if (otroStringList.contains(string)) {
				resultList.add(string);
			}
		}
		Conjunto intersection = new Conjunto(resultList.size());
		for (String string : resultList) {
			intersection.agregarItem(string);
		}
		return intersection;
	}

	/**
	 * Retorna el conjunto diferencia de el Conjunto con el del parametro.
	 * 
	 * @param difference
	 */
	public Conjunto diferencia(Conjunto otroConjunto) {
		ArrayList<String> otroStringList = new ArrayList<String>();
		ArrayList<String> resultList = new ArrayList<String>();
		String[] otroStrings = otroConjunto.getStrings();
		for (String string : otroStrings) {
			if (string != null) {
				otroStringList.add(string);
			}
		}
		for (String string : strings) {
			if (string != null && !otroStringList.contains(string)) {
				resultList.add(string);
			}
		}
		Conjunto differentiated = new Conjunto(resultList.size());
		for (String string : resultList) {
			differentiated.agregarItem(string);
		}
		return differentiated;
	}

	/**
	 * Retorna el {@code size} del {@code Conjunto}.
	 * 
	 * @return int SizeDelConjunto
	 */
	public int dimension() {
		return strings.length;
	}

	/**
	 * Retorna la cantidad de items no nulos que tiene el {@code Conjunto}.
	 * 
	 * @return int CantidadDeNoNulos
	 */
	public int cardinal() {
		int notNull = 0;
		for (String string : strings) {
			boolean condition = string != null;
			notNull = condition ? notNull + 1 : notNull;
		}
		return notNull;
	}

	/**
	 * Despliega los items del conjunto en la consola.
	 */
	public void desplegar() {
		System.out.println();
		for (String string : strings) {
			System.out.print("\t" + string + "\n");
		}
	}

	/**
	 * Agrega un item al primer espacio vacio ({@code null})
	 * 
	 * @param item
	 * @return addedToArray
	 */
	public boolean agregarItem(String item) {
		boolean addedToArray = false;
		for (int c = 0; c < strings.length; c++) {
			if (strings[c] == null) {
				strings[c] = item;
				addedToArray = true;
				break;
			}
		}
		return addedToArray;
	}

	/**
	 * Agrega un item en la posicion indicada por {@code index}.
	 * 
	 * @param item
	 * @param index
	 * @return
	 */
	public boolean agregarItem(String item, int index)
			throws IndexOutOfBoundsException {
		boolean addedToArray = false;
		strings[index] = item;
		addedToArray = true;
		return addedToArray;
	}

	public String[] getStrings() {
		return strings;
	}

	private void initConjuntoStrings(int size) {
		strings = new String[size];
	}

	/**
	 * Verifica que los conjuntos sean equivalentes.
	 * Dos conjuntos son equivalentes si todos sus items son iguales.
	 * @param otroConjunto
	 * @return
	 */
	public boolean esIgualA(Conjunto otroConjunto) {
		boolean containsAll = true;
		ArrayList<String> esteStringList = new ArrayList<String>();
		ArrayList<String> otroStringList = new ArrayList<String>();
		String[] otroStrings = otroConjunto.getStrings();
		for (String string : strings) {
			if (string != null) {
				esteStringList.add(string);
			}
		}
		for (String string : otroStrings) {
			if (string != null) {
				otroStringList.add(string);
			}
		}
		while (containsAll) {
			for (String string : esteStringList) {
				if (!otroStringList.contains(string)) {
					containsAll = false;
				}
			}
			for (String string : otroStringList) {
				if (!esteStringList.contains(string)) {
					containsAll = false;
				}
			}
			break;
		}
		return containsAll;
	}
}
